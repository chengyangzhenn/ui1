import React, {component} from 'react';
import {Cell, Column, Table} from "@blueprintjs/table";

import
{
  Alignment,
  Button,
  Classes,
  H2,
  Colors
} from "@blueprintjs/core";

import './App.css';

export default class TransactionHistory extends React.Component{

  
     render() { //Whenever our class runs, render method will be called automatically, it may have already defined in the constructor behind the scene.
      var rowIndex;
      const cellRenderer = (rowIndex) => {
        return <Cell></Cell>
      };
                 
        return (
          <div>
          <div><H2 className="headings" style = {{fontSize:15, color: Colors.WHITE}} > {"Transaction History"} </H2>            
          </div>
          <div className = "tables">
          <Table numRows={5} className={Classes.DARK}>
            <Column name="Date Time" cellRenderer={cellRenderer} />
            <Column name = "Symbol"/>
            <Column name = "Type" />
            <Column name = "Quantity" />
            <Column name = "Price" />
          </Table>
          </div>
          </div>
        )}
     }
  