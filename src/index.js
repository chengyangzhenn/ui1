import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';

import Navigation  from "./Navigation";
import StocksChart  from "./StocksChart";
import MarketData from "./MarketData";
import Strategies from "./Strategies";
import OrderBook from "./OrderBook";
import Position from "./Position";
import TransactionHistory from "./TransactionHistory";
import Question from "./Question"


const App = () => (
    <div>
        <Navigation />
        <MarketData />
        <Strategies />
        < OrderBook />
        < Position />
        < TransactionHistory />


        

    </div>
);

ReactDOM.render(<App/>, document.getElementById('root'));

