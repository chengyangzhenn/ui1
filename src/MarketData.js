import React, {component} from 'react';
import {Cell, Column, Table, ColumnHeaderCell,EditableName, EditableCell, Regions } from "@blueprintjs/table";

import
{
  Alignment,
  Button,
  Classes,
  H2,
  Icon,
  Intent,
  IconNames, 
  button, 
  Colors, MenuItem
} from "@blueprintjs/core";

import './App.css';
import Question from './Question';
import { color } from 'highcharts';
import { ALIGN_RIGHT } from '@blueprintjs/icons/lib/esm/generated/iconContents';




export default class MarketData extends React.Component{

  state = {
    questions: [
       { id: 'fdsd', title: 'Why is the sky blue?' },
       { id: 'adsf', title: 'Who invented pizza?' },
       { id: 'afdsf', title: 'Is green tea overrated?' },
    ],
       displayQuestions: false
  }

  displayQuestion = () => { //function to display questions on click
    this.setState({
          displayQuestions : !this.state.displayQuestions
            })
          }  
  
     render() {  
      var rowIndex;
      var questions = null;
      const cellRenderer = (rowIndex) => {
        return <Cell></Cell>
      };

      if ( this.state.displayQuestions ) { // if displayQuestions is true
        questions = (

        //map the questions with key being a unique number to identity elements in your array
        <div>
             { this.state.questions.map((question, index) => {
                  return <Question key={question.id} title={question.title} />
             })}
        </div>
        )  }

        return (
    <div>
        <div>
          <H2 className = "headings" style ={{fontSize: 15, color: Colors.WHITE}}> {"Market Data"} </H2>
          <Button icon = "add" id = "icon" onClick = {this.displayQuestion} variant = "primary"> Watchlist</Button> 
          <p id = "questions"> {questions}</p>

        </div>
        <div className = "tables">
          <Table numRows={5} className={Classes.DARK}>
            <Column name="Symbol" cellRenderer={cellRenderer}/>
            <Column className = "columns" name = "Market Price" />
            <Column className = "columns" name = "Change"  />
            </Table>
        </div>
     </div>

        )}
     }
    