import React, {component} from 'react';
import {Cell, Column, Table, ColumnHeaderCell, RowHeaderCell} from "@blueprintjs/table";

import
{
  Alignment,
  Button,
  Classes,
  H2,
  Colors, Icon
} from "@blueprintjs/core";

import './App.css';
import { Color } from 'highcharts';

export default class Strategies extends React.Component{

  
     render() { //Whenever our class runs, render method will be called automatically, it may have already defined in the constructor behind the scene.
      var rowIndex;
      const cellRenderer = (rowIndex) => {
        return <Cell> </Cell>
      };

      const starttrade = () => {
        return <Cell>  </Cell>
      }
                 
        return (
          <div>
          <div>
          <H2 className="headings" style = {{fontSize:15, color: Colors.WHITE}}> {"Strategies"} </H2> 
          </div>
          <div className = "tables">
          <Table numRows={5} className={Classes.DARK}>
            <Column name="Strategy Name" cellRenderer={cellRenderer} />
            <Column name = "Symbol"/>
            <Column name = "Money Invested" />
            <Column name = "Status" />
            <Column name = "PnL" />
            <Column name = "Net" />
            <Column name = "Actions">  
            <button Icon = "play" id ="strategybutton1"> Start Trade </button> 
            <button Icon = "pause" id = "strategybutton2" />
            </Column>
          </Table>
          </div>
          </div>
        )}
     }
  