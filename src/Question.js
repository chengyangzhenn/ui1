import React, {component} from 'react';

const Question = ( props ) => {
  return (
   <div className="Question">
    <p>{props.title}</p>
   </div>
  )
 };
 
 export default Question;